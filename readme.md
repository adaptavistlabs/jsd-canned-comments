# Template Comments for JIRA Service Desk

## Overview.

This plugin is for [ScriptRunner for JIRA](https://marketplace.atlassian.com/plugins/com.onresolve.jira.groovy.groovyrunner/server/overview) users, and provides configurable template comments for replying to customers.

The templates can use attributes of the current issue, linked issues, the reporter and current user, to create personalised comments.

Administrators can set up different available templates for different projects, or roles etc.

You can also specify a template to be used by default.

## Requirements

* JIRA 7.x
* [ScriptRunner for JIRA](https://marketplace.atlassian.com/plugins/com.onresolve.jira.groovy.groovyrunner/server/overview)
* [JIRA Service Desk application](https://www.atlassian.com/software/jira/service-desk)

## Usage

Upon installation, a few sample templates will be installed. You can edit or delete them as appropriate.

To add more, go to *Admin -> Built-in Scripts -> Service Desk template comments*.

You must fill in the _Name_ and _Template_. The _name_ is shown to the user in the dropdown box, to allow them to select that comment. 

![Comment dropdown](site/img/comments.png)

The template is processed by [GStringTemplateEngine](http://docs.groovy-lang.org/latest/html/api/groovy/text/GStringTemplateEngine.html). To see the variables available to use, expand the Examples link, and click _Kitchen Sink_. Select a sample issue to preview the template with, and click *Preview*. You should see something like the following:

![Comment dropdown](site/img/kitchensink.png)

There is an additional string you can use in your template: `{CURSOR}`. This is not a variable so should not have a dollar sign. If found, the caret will be placed at this position within the comment text box. See the "default" template for an example.

### Snippets And Default

If a comment is marked as a a _snippet_, it will be inserted at the current cursor position in the comment. If not, it will set or replace the entire comment.

A good strategy is to have a single default comment, with the cursor placed between the greeting and sign-off, and then the rest of the templates be snippets.

That way you can form the response from multiple snippets.

### Conditions

The *Condition / Code* section controls two things 
 
* whether the template will be available for selection by the user
* additional variables available to the template

To have different comments for different projects, you could use a condition such as:

    issue.projectObject.key == 'ABC'

The *Condition / Code* section can also provide additional variables to the template.
 
In this example, we retrieve the value of a custom field and provide it in the template:

    import com.atlassian.jira.component.ComponentAccessor
    
    def customFieldManager = ComponentAccessor.getCustomFieldManager()
    def refundCf = customFieldManager.getCustomFieldObjectByName("Refund Amount")
    
    config.refund = issue.getCustomFieldValue(refundCf) ?: 0
    
    return true // must return true otherwise the comment will be hidden

The template may look like:

    Dear $customerFirstName,
    
    We have processed your claim and you are entitled to a refund of
    
    USD $refund
    
    Nice!

## Notes

Service Desk [conversational mode](https://jira.atlassian.com/browse/JSD-2257) is automatically disabled when using this plugin.

## Development

    mvn jira:debug

or

    atlas-run

If you are modifying the javascript change run `mvn jira:debug "-Ddev.mode=true"` to enable dev mode.

If you change the REST endpoint run `mvn package` to rebuild. _Quick reload_ will install the plugin.

## Releasing

	mvn release:prepare -Darguments="-DskipTests"
	mvn release:perform -Darguments="-DskipTests"
