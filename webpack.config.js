const debug = process.env.NODE_ENV !== "production"; //From environment variables
const webpack = require('webpack');
const path = require('path');


module.exports = function (env) {
    console.log("env", env); //From command line args
    console.log("dirname", __dirname);

    return {
        resolve: {
            modules: ['node_modules', './src/main/resources/js']

        },

        context: __dirname,
        devtool: debug ? "inline-sourcemap" : null, //Can debug be removed and use env instead?
        entry: {
            "sd-canned-responses": [
                "sd-canned-responses.js"
            ]
        },
        output: {
            path: __dirname + "/build/resources/main/public/js/",
            publicPath: "http://localhost:8099/js/",
            filename: '[name].js'
        },

        externals: [{
            'ScriptRunner': 'window.ScriptRunner',
            'AJS.$': 'AJS.$',
            "jquery": "jQuery",
            "lodash": "_",
            "VERSION": "VERSION",
            "aui/flag": "aui/flag"
        }],

        module: {
            rules: [
                {
                    include: [
                        path.resolve(__dirname, 'spec/'),
                        path.resolve(__dirname, "build/resources/main/public/js"),
                    ],
                    loader: 'babel-loader'
                },
            ]
        },

        devServer: {
            port: 8099,
            hot: true,
            inline: true,
            contentBase: [
                "/"
            ],
            headers: {
                'Access-Control-Allow-Origin': '*'
            }
        },

        plugins: [
            new webpack.ProvidePlugin({
                // "window.plugin": "plugin",
                "window.ScriptRunner": "ScriptRunner",
                "$": "AJS.$",
                // jQuery: "jquery",
                // "window.jQuery": "jquery"
            }),
            new webpack.NamedModulesPlugin(),
        ]
    }
};

