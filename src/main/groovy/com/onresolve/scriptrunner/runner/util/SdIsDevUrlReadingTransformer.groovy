package com.onresolve.scriptrunner.runner.util

import com.atlassian.plugin.elements.ResourceLocation
import com.atlassian.plugin.servlet.DownloadException
import com.atlassian.plugin.servlet.DownloadableClasspathResource
import com.atlassian.plugin.servlet.DownloadableResource
import com.atlassian.plugin.webresource.QueryParams
import com.atlassian.plugin.webresource.WebResourceIntegration
import com.atlassian.plugin.webresource.transformer.*
import com.atlassian.plugin.webresource.url.UrlBuilder
import groovy.util.logging.Log4j
import org.apache.commons.io.IOUtils

import javax.servlet.http.HttpServletRequest
import javax.servlet.http.HttpServletResponse
import java.nio.charset.StandardCharsets

@Log4j
class SdIsDevUrlReadingTransformer implements WebResourceTransformerFactory {

    private final WebResourceIntegration webResourceIntegration
    private boolean isDevMode = false

    SdIsDevUrlReadingTransformer(WebResourceIntegration webResourceIntegration) {
        this.webResourceIntegration = webResourceIntegration
        isDevMode = Boolean.getBoolean("adaptavist.dev.mode")
    }

    @Override
    TransformerUrlBuilder makeUrlBuilder(TransformerParameters parameters) {
        new TransformerUrlBuilder() {
            @Override
            void addToUrl(UrlBuilder urlBuilder) {
                null
            }
        }
    }

    @Override
    UrlReadingWebResourceTransformer makeResourceTransformer(TransformerParameters parameters) {
        new UrlReadingWebResourceTransformer() {
            @Override
            DownloadableResource transform(TransformableResource transformableResource, QueryParams params) {
                def location = transformableResource.location()
                def plugin = webResourceIntegration.getPluginAccessor().getPlugin(parameters.getPluginKey())
                def path = location.location.replaceAll(/.switchable$/, "")
                def prodResourceLocation = new ResourceLocation(path, location.name, location.type, location.contentType, location.content, location.params)

                if (!isDevMode) return new DownloadableClasspathResource(plugin, prodResourceLocation, "")

                path = path.replaceAll("/public/", "") // hack, todo
                new DownloadableResource() {
                    @Override
                    boolean isResourceModified(HttpServletRequest request, HttpServletResponse response) {
                        return false
                    }

                    @Override
                    void serveResource(HttpServletRequest request, HttpServletResponse response) throws DownloadException {
                        throw new IllegalStateException("Not implemented, appears not to be required")
                    }

                    @Override
                    void streamResource(OutputStream out) throws DownloadException {
                        def is
                        if (location.name.endsWith(".css")) {
                            is = new ByteArrayInputStream("Using CSS from JS Bundle in Dev Mode".getBytes(StandardCharsets.UTF_8))
                        } else {
                            is = new ByteArrayInputStream("document.write(\"<script src='http://localhost:8099/$path'></script>\");".getBytes(StandardCharsets.UTF_8))
                        }
                        try {
                            IOUtils.copy(is, out)
                        }
                        catch (IOException e) {
                            throw new DownloadException(e)
                        }
                        finally {
                            IOUtils.closeQuietly(is)
                            try {
                                out.flush()
                            }
                            catch (IOException e) {
                                log.debug("Error flushing output stream", e)
                            }
                        }
                    }

                    @Override
                    String getContentType() {
                        location.contentType
                    }
                }
            }
        }
    }
}
