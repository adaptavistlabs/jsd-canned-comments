package com.adaptavist.jsdcc.rest

import com.adaptavist.jsdcc.CannedCommentsService
import com.atlassian.jira.component.ComponentAccessor
import com.atlassian.jira.issue.IssueManager
import com.atlassian.jira.issue.MutableIssue
import com.atlassian.jira.permission.Permission
import com.atlassian.jira.permission.ProjectPermissions
import com.atlassian.jira.security.PermissionManager
import com.atlassian.jira.user.ApplicationUser
import groovy.json.JsonBuilder
import groovy.transform.CompileStatic
import groovy.util.logging.Log4j

import javax.ws.rs.GET
import javax.ws.rs.Path
import javax.ws.rs.Produces
import javax.ws.rs.QueryParam
import javax.ws.rs.core.MediaType
import javax.ws.rs.core.Response

@Path("/jsd/comments")
@CompileStatic
@Log4j
class CommentsEndpoint {

    private final CannedCommentsService cannedCommentsService
    private final PermissionManager permissionManager
    private final IssueManager issueManager

    CommentsEndpoint(PermissionManager permissionManager, IssueManager issueManager) {
        this.cannedCommentsService = new CannedCommentsService()
        this.permissionManager = permissionManager
        this.issueManager = issueManager
    }

    @GET
    @Path("/keys")
    @Produces([MediaType.APPLICATION_JSON])
    Response getKeys(
        @QueryParam("issueId") Long issueId,
        @QueryParam("actionId") Integer actionId,
        @QueryParam("formId") String formId
    ) {

        def errorResponse = checkIssue(issueId)
        if (errorResponse) {
            return errorResponse
        }

        def comments = cannedCommentsService.filter(issueId, actionId, formId)

        def rt = comments.collect { Map it ->
            [
                id     : it.id,
                name   : it.name,
                snippet: it.snippet as boolean,
                default: it.default as boolean,
            ]
        }
        return Response.ok(new JsonBuilder(rt).toString()).build();
    }

    @GET
    @Path("/comment")
    @Produces([MediaType.APPLICATION_JSON])
    Response processComment(
        @QueryParam("id") Integer id,
        @QueryParam("issueId") Long issueId,
        @QueryParam("actionId") Integer actionId,
        @QueryParam("formId") String formId
    ) {

        def errorResponse = checkIssue(issueId)
        if (errorResponse) {
            return errorResponse
        }

        def comment = cannedCommentsService.get(id)

        def processedComment = cannedCommentsService.processComment(issueId, actionId, formId, comment)

        return Response.ok(new JsonBuilder([
            template: processedComment,
            snippet: comment.snippet as boolean,
        ]).toString()).build()
    }

    private Response checkIssue(Long issueId) {
        def issue = issueManager.getIssueObject(issueId)
        if (!issue) {
            return Response.status(Response.Status.NOT_FOUND).entity("Cannot find that issue").build()
        }

        def user = ComponentAccessor.getJiraAuthenticationContext().getLoggedInUser()
        if (!permissionManager.hasPermission(ProjectPermissions.BROWSE_PROJECTS, issue, user)) {
            return Response.status(Response.Status.FORBIDDEN).entity("Cannot view that issue").build()
        }
        null
    }
}
