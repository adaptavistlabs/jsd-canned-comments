(function ($, AJS, _) {

    function insertComentTiny(ed, textToSet, endId) {
        if (textToSet.includes("{CURSOR}")) {
            textToSet = textToSet.replace("{CURSOR}", "<p id=\"" + endId + "\"></p>");
            ed.setContent(textToSet, {format: 'text'});
            var node = ed.dom.select('p#' + endId)[0];
            ed.selection.select(node);
        }
        else {
            ed.selection.setContent(textToSet, {format: 'text'});
        }
        ed.undoManager.add();
        ed.focus();
    }

    function fillComment(commentId, $context, overwriteComment) {
        var cursorPos;

        $("#comment").off("focus");

        if (commentId === "") {
            return;
        }
        var $comment = $context.find("#comment");

        // only fill comment if empty
        if (!!$comment.val() && !overwriteComment) {
            return;
        }

        var queryParams = getQueryParams($context);
        if (!queryParams) {
            return;
        }
        queryParams["id"] = commentId;

        $.ajax({
            type: "GET",
            url: AJS.contextPath() + "/rest/jsdcanned/1.0/jsd/comments/comment",
            data: queryParams
        }).fail(function (XMLHttpRequest, textStatus, errorThrown) {
            console.warn("Failed to get response", errorThrown);
        }).done(function (cannedResponse) {
            var template = cannedResponse.template;

            var stringToSet;

            if ($comment.hasClass("richeditor-cover")) {
                var postdata = {
                    rendererType: "atlassian-wiki-renderer",
                    unrenderedMarkup: template
                };
                $.ajax({
                    type: "POST",
                    url: AJS.contextPath() + "/rest/api/1.0/render",
                    contentType: "application/json",
                    data: JSON.stringify(postdata)
                }).error(function (data) {
                    console.log("error fetching data:  " + data);
                }).done(function (data) {
                    var textToSet = data;
                    var ed = tinyMCE.activeEditor;
                    var endId = tinyMCE.DOM.uniqueId();
                    try {
                        insertComentTiny(ed, textToSet, endId);
                    }
                    catch (err) {
                        ed.on('init', function (args) {
                            insertComentTiny(ed, textToSet, endId);
                        });
                    }
                });
            }
            else {
                $comment.focus();
                if (cannedResponse["snippet"]) {
                    cursorPos = $("#comment").prop('selectionStart');

                    var v = $comment.val();
                    var textBefore = v.substring(0, cursorPos);
                    var textAfter = v.substring(cursorPos, v.length);
                    stringToSet = textBefore + template + textAfter;

                }
                else {
                    stringToSet = template;
                }
                cursorPos = stringToSet.indexOf("{CURSOR}");
                stringToSet = stringToSet.replace("{CURSOR}", "");
                $comment.val(stringToSet);
                if (cursorPos > -1) {
                    $comment.setSelection(cursorPos, cursorPos);
                }
                else {
                    cursorPos = textBefore.length + template.length + 1;
                    $comment.setSelection(cursorPos, cursorPos);
                }
            }
        });
    }


    function getQueryParams($context) {
        var issueId;

        try {
            issueId = JIRA.Issue.getIssueId();
            if (!issueId) {
                return;
            }
        } catch (e) {
            // there is a jira bug here when going to the project home page in recent versions, in LegacyUtils.js
            return;
        }

        var actionId = $context.find("input[name=action]").val();
        return {
            issueId: issueId,
            actionId: actionId,
            formId: $context.find("form").attr("id")
        };
    }

    function addCannedComments($context) {

        var queryParams = getQueryParams($context);
        if (!queryParams) {
            return;
        }

        $.ajax({
            type: "GET",
            url: AJS.contextPath() + "/rest/jsdcanned/1.0/jsd/comments/keys",
            data: queryParams
        }).fail(function (XMLHttpRequest, textStatus, errorThrown) {
            console.warn("Failed to get responses", errorThrown);
        }).done(function (data) {

            if ($(".canned-responses").length > 0) {
                return
            }

            if (data.length === 0) {
                // nothing applicable
                return;
            }

            var defaults = _.where(data, {"default": true});


            var html = $(SR.CannedComments.Templates.Params.commentsDropDown({
                comments: data
            }));

            $context.find(".sd-add-comment-container").prepend(html);
            $context.find(".sd-comment-message").before(html);

            if (defaults.length > 0) {
                if ($context.is(".jira-dialog-content") && !$context.parent().is("#comment-add-dialog")) {
                    // done: the intention here is that, if this is a dialog, the default comment is
                    // only entered if the user clicks the controls. Otherwise a comment with the default template is always added

                    var $comment = $context.find(".sd-comment-form-container");
                    $comment.one("click", function () {
                        fillComment(defaults[0].id, $context, false);
                    })
                }
                else {
                    fillComment(defaults[0].id, $context, false);
                }
            }

            html.find("a.canned-response-trigger").off('click').on('click', function (e) {
                fillComment(e.target.rel, $context, true);
            });
        });
    }

    function checkForCommentsAndAdd($context) {
        if ($context.find(".sd-add-comment-container #comment:visible").length > 0 || $context.find(".sd-comment-message").length > 0) {
            addCannedComments($context);
        }
    }

    JIRA.bind(JIRA.Events.NEW_CONTENT_ADDED, function (event, $context, reason) {
        if (reason === JIRA.CONTENT_ADDED_REASON.pageLoad || reason === JIRA.CONTENT_ADDED_REASON.panelRefreshed) {
            checkForCommentsAndAdd($context);
        }
    });

    AJS.$(document).bind('dialogContentReady', function (event, dialog) {
        var $context = dialog.$popupContent;
        checkForCommentsAndAdd($context);
    });

})(AJS.$, AJS, _);

if (typeof(module) !== "undefined" && module.hot) {
    console.log("hmr available");

    module.hot.accept(function () {
        console.log("accepted");
        checkForCommentsAndAdd($("body"))
    });

    module.hot.dispose(function () {
        $("#sr-comments-dropdown").remove();
        console.log("dispose");
    });
}

