package com.adaptavist.jsdcc

import com.atlassian.plugin.PluginAccessor
import com.atlassian.sal.api.lifecycle.LifecycleAware
import groovy.util.logging.Log4j

/**
 * Check that we have a late enough version of ScriptRunner.
 *
 * If not write a log message, that's about all we can do.
 *
 * This doesn't work anyway because we can't get past the fact that the exports are not present for previous versions
 */
@Log4j
class SrVersionChecker implements LifecycleAware {

    private final PluginAccessor pluginAccessor

    SrVersionChecker(PluginAccessor pluginAccessor) {
        this.pluginAccessor = pluginAccessor
    }

    void onStart() {
        def plugin = pluginAccessor.getPlugin(CannedCommentsService.SR_PLUGIN_KEY)

        // plugin must be present because it's required... but check anyway
        if (!plugin) return

        if (!(versionCompare(plugin.pluginInformation.version, "4.3.7") >= 0)) {
            log.fatal "*****************************************"
            log.fatal "Template Comments requires at ScriptRunner at least 4.3.7... please upgrade ScriptRunner, " +
                "or remove Template Comments (key is ${CannedCommentsService.PLUGIN_KEY})"
            log.fatal "*****************************************"
        }
    }

    void onStop() { }

    // http://stackoverflow.com/questions/6701948/efficient-way-to-compare-version-strings-in-java
    static int versionCompare(String str1, String str2) {
        String[] vals1 = str1.split("\\.");
        String[] vals2 = str2.split("\\.");
        int i = 0;
        // set index to first non-equal ordinal or length of shortest version string
        while (i < vals1.length && i < vals2.length && vals1[i] == vals2[i]) {
            i++;
        }
        // compare first non-equal ordinal number
        if (i < vals1.length && i < vals2.length) {
            int diff = Integer.valueOf(vals1[i]) <=> Integer.valueOf(vals2[i]);
            return Integer.signum(diff);
        }
        // the strings are equal or one string is a substring of the other
        // e.g. "1.2.3" = "1.2.3" or "1.2.3" < "1.2.3.4"
        return Integer.signum(vals1.length - vals2.length);
    }
}
