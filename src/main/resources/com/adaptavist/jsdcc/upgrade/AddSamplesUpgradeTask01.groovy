package com.adaptavist.jsdcc.upgrade

import com.adaptavist.jsdcc.CannedCommentsService
import com.atlassian.sal.api.message.Message
import com.atlassian.sal.api.upgrade.PluginUpgradeTask

class AddSamplesUpgradeTask01 implements PluginUpgradeTask {

    int getBuildNumber() {
        return 1
    }

    String getShortDescription() {
        "Add sample default templates"
    }

    Collection<Message> doUpgrade() throws Exception {
        def service = new CannedCommentsService()

        service.add([
            name    : "Provide logs...",
            snippet : true,
            template: '''\
                Please could you add the application server logs after you have reproduced this problem.
    
                You can find out how to get the server logs for 
                
                * [JIRA|https://confluence.atlassian.com/jira/where-are-the-application-server-logs-16121981.html], 
                * [Confluence|https://confluence.atlassian.com/doc/working-with-confluence-logs-108364721.html], 
                * [Bitbucket|https://confluence.atlassian.com/bitbucketserver/bitbucket-server-home-directory-776640890.html].
    
                It may be helpful to add screenshots too.'''.stripIndent(),
        ])

        service.add([
            name    : "Bug created (known issue)...",
            snippet : true,
            template: '''\
                Thank you for reporting this problem. We have created a bug report and added it to our backlog (<% out << (causedByIssues*.key.join(", ") ?: "_if you had linked the issue first you wouldn't have to type it in after_") %>).
    
                Please _watch_ it to be informed of when it's fixed and released.'''.stripIndent(),
        ])

        service.add([
            name    : "Issue resolved...",
            snippet : true,
            template: '''\
                Thank you for reporting this issue.

                We are pleased to tell you the issue has been resolved in version [${config.fixVersion}|${config.releaseNotes}].'''.stripIndent(),

            condition: '''\
                import com.atlassian.jira.component.ComponentAccessor
                import com.atlassian.jira.config.properties.APKeys
                
                if (causedByIssues) {
                    def fixVersions = causedByIssues.first().fixVersions
                    if (fixVersions) {
                        def version = fixVersions.first()
                        def baseUrl = ComponentAccessor.getApplicationProperties().getString(APKeys.JIRA_BASEURL)
                
                        config["fixVersion"] = version.name
                        config["releaseNotes"] = "${baseUrl}/browse/${version.project.key}/fixforversion/${version.id}"
                
                        return version.isReleased()
                    }
                }
                
                false
            '''.stripIndent(),
        ])

        service.add([
            name    : "Default template",
            default : "true",
            snippet : false,
            template: '''\
                Hi ${customerFirstName},

                {CURSOR}
    
                Regards, ${agentFirstname}'''.stripIndent(),
        ])

        return null
    }

    String getPluginKey() {
        CannedCommentsService.PLUGIN_KEY
    }
}
