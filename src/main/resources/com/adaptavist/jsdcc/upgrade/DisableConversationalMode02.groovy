package com.adaptavist.jsdcc.upgrade

import com.adaptavist.jsdcc.CannedCommentsService
import com.atlassian.jira.component.ComponentAccessor
import com.atlassian.jira.config.FeatureManager
import com.atlassian.jira.config.FeatureStore
import com.atlassian.jira.config.feature.DefaultFeatureManager
import com.atlassian.jira.event.ClearCacheEvent
import com.atlassian.sal.api.message.Message
import com.atlassian.sal.api.upgrade.PluginUpgradeTask

class DisableConversationalMode02 implements PluginUpgradeTask {

    int getBuildNumber() {
        return 2
    }

    String getShortDescription() {
        "Disables service desk conversational mode"
    }

    /*
    Hacky way to set a dark feature without a user context
     */
    Collection<Message> doUpgrade() throws Exception {
        def featureManager = ComponentAccessor.getComponent(FeatureManager)

        if (!featureManager.isEnabled("sd.email.conversational.notification.disabled")) {

            def featureStore = ComponentAccessor.getComponent(FeatureStore)
            featureStore.create("sd.email.conversational.notification.disabled", (String) null)
            (featureManager as DefaultFeatureManager).onClearCache(new ClearCacheEvent([:]))
        }

        null
    }

    String getPluginKey() {
        CannedCommentsService.PLUGIN_KEY
    }
}
