package com.adaptavist.jsdcc

import com.atlassian.jira.component.ComponentAccessor
import com.atlassian.jira.issue.Issue
import com.atlassian.jira.issue.IssueManager
import com.atlassian.jira.issue.fields.screen.FieldScreenManager
import com.atlassian.jira.issue.link.IssueLinkManager
import com.atlassian.jira.workflow.WorkflowManager
import com.atlassian.sal.api.pluginsettings.PluginSettingsFactory
import com.onresolve.scriptrunner.canned.jira.utils.ConditionUtils
import com.onresolve.scriptrunner.runner.ContextAndEvalResult
import com.onresolve.scriptrunner.runner.ScriptRunnerImpl
import com.onresolve.scriptrunner.runner.stc.ScriptCompileContext
import com.opensymphony.workflow.loader.ActionDescriptor
import groovy.json.JsonOutput
import groovy.json.JsonSlurper
import groovy.text.GStringTemplateEngine
import groovy.util.logging.Log4j
import org.codehaus.groovy.ast.ClassHelper
import org.codehaus.groovy.ast.GenericsType
import org.codehaus.groovy.ast.tools.GenericsUtils

import javax.script.ScriptContext

@Log4j
class CannedCommentsService {

    public static final String PLUGIN_KEY = "com.onresolve.scriptrunner.assets.jsd-canned-comments"
    public static final String SR_PLUGIN_KEY = "com.onresolve.jira.groovy.groovyrunner"

    public static
    final ScriptCompileContext CONFIG_AND_CONDITION_CONTEXT = ConditionUtils.CONDITION_AND_ADDITIONAL_CONTEXT + new ScriptCompileContext(
        bindingVariables: [
            config           : GenericsUtils.makeClassSafeWithGenerics(ClassHelper.MAP_TYPE, new GenericsType(ClassHelper.STRING_TYPE), new GenericsType(ClassHelper.OBJECT_TYPE)),
            actionId         : ClassHelper.Integer_TYPE,
            agentFirstname   : ClassHelper.STRING_TYPE,
            customerFirstName: ClassHelper.STRING_TYPE,
            causedByIssues   : GenericsUtils.makeClassSafeWithGenerics(ClassHelper.LIST_TYPE, new GenericsType(ClassHelper.make(Issue))),
            operation        : ClassHelper.STRING_TYPE,
            getActionName    : ClassHelper.CLOSURE_TYPE,
            getScreenName    : ClassHelper.CLOSURE_TYPE,
        ],

        name: "Condition for processing an issue, and allows setting a map of variables for a template",
        label: CannedCommentsService.name + ".CONFIG_AND_CONDITION_CONTEXT"
    )

    private final PluginSettingsFactory pluginSettingsFactory
    private final IssueManager issueManager
    private final IssueLinkManager issueLinkManager
    private final WorkflowManager workflowManager
    private final FieldScreenManager screenManager


    CannedCommentsService() {
        this.pluginSettingsFactory = ScriptRunnerImpl.getOsgiService(PluginSettingsFactory)
        this.issueManager = ComponentAccessor.getComponent(IssueManager)
        this.issueLinkManager = ComponentAccessor.getComponent(IssueLinkManager)
        this.workflowManager = ComponentAccessor.getWorkflowManager()
        this.screenManager = ComponentAccessor.getComponent(FieldScreenManager)
    }

    Map get(Integer id) {
        all().get(id)
    }

    List<Map> filter(Long issueId, Integer actionId, String formId) {
        def issue = issueManager.getIssueObject(issueId)

        def binding = getBinding(issue, actionId, formId)

        all().findAll {
            def condition = it.condition as String
            if (ConditionUtils.methods.any { it.name == "processCondition"}) {
                return ConditionUtils.processCondition(condition, issue, false, binding)
            }

            def contextAndEvalResult = ConditionUtils.processConditionAndContext(getCustomScript(condition), issue, false, binding)
            contextAndEvalResult.result
        }
    }

    void add(Map comment) {
        def m = all()
        m.add(comment)
        save(m)
    }

    def save(List<Map> m) {
        def settings = pluginSettingsFactory.createGlobalSettings()
        settings.put(PLUGIN_KEY, JsonOutput.toJson(m))
    }

    List<Map> all() {
        def settings = pluginSettingsFactory.createGlobalSettings()

        String str = settings.get(PLUGIN_KEY)

        if (str) {
            def list = new JsonSlurper().parseText(str) as List<Map>

            def kount = 0
            return list.collect {
                it.id = kount++
                it
            }
        } else {
            []
        }
    }

    String processComment(Long issueId, Integer actionId, String formId, Map comment) {
        // expect an issue ID and the key of the comment to get.
        def issue = issueManager.getIssueObject(issueId)

        def engine = new GStringTemplateEngine()

        def initialBindings = getBinding(issue, actionId, formId)

        def condition = comment.condition as String

        if (condition) {
            ContextAndEvalResult context
            if (ConditionUtils.methods.any { it.name == "processCondition"}) {
                context = ConditionUtils.processConditionAndContext(condition, issue, true, initialBindings)
            } else {
                context = ConditionUtils.processConditionAndContext(getCustomScript(condition), issue, true, initialBindings)
            }

            if (!(context.result as boolean)) {
                return null
            }

            def bindings = context.scriptContext.getBindings(ScriptContext.ENGINE_SCOPE)
            if (bindings.get("config") instanceof Map) {
                def config = bindings.get("config") as Map
                initialBindings << config
            }
        }

        def text = comment.template ?: "Template not found"
        def template = engine.createTemplate(text.stripIndent()).make(initialBindings)
        template.toString()
    }

    private Object getCustomScript(String condition) {
        def clazz = this.class.classLoader.loadClass("com.onresolve.scriptrunner.canned.jira.utils.CustomScript")
        def customScript = clazz.newInstance([inlineScript: condition])
        customScript
    }

    private LinkedHashMap<String, Object> getBinding(Issue issue, Integer actionId, String formId) {

        def currentUser = ComponentAccessor.jiraAuthenticationContext.getLoggedInUser()
        def agentFirstname = currentUser.displayName.replaceAll(/\s.*/, '')
        def customerFirstName = issue.reporter?.displayName?.replaceAll(/\s.*/, '') ?: "Anonymous"

        def causedByIssues = issueLinkManager.getInwardLinks(issue.id).findAll {
            it.issueLinkType.name in ["Problem/Incident"]
        }*.sourceObject

        // this is like this because someone has set up the Cause link type back to front in PS
        if (!causedByIssues) {
            causedByIssues = issueLinkManager.getOutwardLinks(issue.id).findAll {
                it.issueLinkType.name in ["Cause"]
            }*.destinationObject
        }

        // NOTE: Add any new variables to the compile context at the top
        Map initialBindings = [
            currentUser      : currentUser,
            issue            : issue,
            agentFirstname   : agentFirstname,
            customerFirstName: customerFirstName,
            causedByIssues   : causedByIssues,
            config           : [:],
            actionId         : actionId,
            operation        : formId,
            getActionName    : { ->
                getAction(issue, actionId)?.name
            },
            getScreenName    : { ->
                getScreenForAction(issue, actionId)
            },
        ]

        initialBindings
    }

    private ActionDescriptor getAction(Issue issue, Integer actionId) {
        if (!actionId) {
            return null
        }
        def workflow = workflowManager.getWorkflow(issue)
        workflow.descriptor.getAction(actionId)
    }

    private String getScreenForAction(Issue issue, Integer actionId) {
        if (!actionId) {
            return null
        }

        def action = getAction(issue, actionId)
        String screenId = action.getMetaAttributes().get("jira.fieldscreen.id")
        def screen = screenManager.getFieldScreen(screenId as Long)
        screen.name
    }
}
