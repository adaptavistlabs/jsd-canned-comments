package com.onresolve.base.test.rest.jsd

import com.adaptavist.jsdcc.CannedCommentsService
import com.atlassian.jira.component.ComponentAccessor
import com.atlassian.jira.issue.link.IssueLinkManager
import com.atlassian.sal.api.pluginsettings.PluginSettingsFactory
import com.onresolve.scriptrunner.runner.ScriptRunnerImpl
import com.onresolve.scriptrunner.runner.rest.common.CustomEndpointDelegate
import groovy.json.JsonOutput
import groovy.json.JsonSlurper
import groovy.text.GStringTemplateEngine
import groovy.transform.BaseScript

import javax.ws.rs.core.MediaType
import javax.ws.rs.core.MultivaluedMap
import javax.ws.rs.core.Response

@BaseScript CustomEndpointDelegate delegate

def msgs = [ // <1>
    "Provide logs..."             : '''\
            Hi ${customerFirstName},

            Please could you add the application server logs after you have reproduced this problem.

            You can find out how to get the server logs for [JIRA|https://confluence.atlassian.com/jira/where-are-the-application-server-logs-16121981.html], for [Confluence|https://confluence.atlassian.com/doc/working-with-confluence-logs-108364721.html], or for [Bitbucket|https://confluence.atlassian.com/bitbucketserver/bitbucket-server-home-directory-776640890.html].

            It may be helpful to add screenshots too.

            Regards, ${agentFirstname}''',

    "Bug created (known issue)...": '''\
            Hi ${customerFirstName},

            Thank you for reporting this problem. We have created a bug report and added it to our backlog (<% out << (causedByIssues*.key.join(", ") ?: "_if you had linked the issue first you wouldn't have to type it in after_") %>).

            Please _watch_ it to be informed of when it's fixed and released.

            Regards, ${agentFirstname}''',

    "So happy you fixed it..."    : '''\
            Hi ${customerFirstName},

            Great - we're happy you were able to fix it!

            Thanks for letting us know.

            Regards, ${agentFirstname}''',

     "Add us to Atlassian ticket..."    : '''\
            Hi ${customerFirstName},

            Sorry to hear you are having a problem. Please could you add us as _Request Participants_ to your ticket on _support.atlassian.com_.

            Thanks for letting us know.

            Regards, ${agentFirstname}''',

    "Ask on Answers..." : '''\
            Hi ${customerFirstName},

            Although we would like to help with all coding questions, unfortunately time does not allow us to. Custom coding questions, by their very nature, are limitless, so this is not covered by our EULA.

            We recommend you ask your question on [Atlassian Answers|https://answers.atlassian.com] where there is a very active community. Adaptavist staff are also likely to respond there.

            Please tag your question with the ScriptRunner plugin tag, which is:

            * for JIRA: {{com.onresolve.jira.groovy.groovyrunner}}
            * for Confluence: {{com.onresolve.jira.confluence.groovyrunner}}
            * for Bitbucket Server: {{com.onresolve.jira.stash.groovyrunner}}

            Alternatively you can ask your question from the Atlassian Answers link on the relevant plugin's marketplace listing page.

            Regards, ${agentFirstname}'''

]

def issueManager = ComponentAccessor.getIssueManager()
def issueLinkManager = ComponentAccessor.getComponent(IssueLinkManager)

List getSettings() {
    def pluginSettingsFactory = ScriptRunnerImpl.getOsgiService(PluginSettingsFactory)

    log.debug(pluginSettingsFactory)

    def settings = pluginSettingsFactory.createGlobalSettings()

    def str = settings.get(CannedCommentsService.PLUGIN_KEY)

    if (str) {
        def list = new JsonSlurper().parseText(str) as List

        def kount = 0
        list.collect {
            it.put("id", kount++)
            it
        }
        list
    }
    else {
        []
    }
}

getCannedCommentKeys(httpMethod: "GET", groups: ["service-desk-agents", "jira-servicedesk-users"]) { MultivaluedMap queryParams ->
    def issueId = queryParams.getFirst("issueId") as Long

    // can filter on issue if required...
    return Response.ok(JsonOutput.toJson(getSettings()*.name)).build();
}

getCannedComment(httpMethod: "GET", groups: ["service-desk-agents", "jira-servicedesk-users"]) { MultivaluedMap queryParams ->

    // expect an issue ID and the key of the comment to get.
    def issueId = queryParams.getFirst("issueId") as Long
    String msgKey = queryParams.getFirst("msgKey")

    def issue = issueManager.getIssueObject(issueId)

    // Comments will be run through gstring template engine
    def engine = new GStringTemplateEngine()

    def currentUser = ComponentAccessor.jiraAuthenticationContext.getLoggedInUser()
    def agentFirstname = currentUser.displayName.replaceAll(/\s.*/, '')
    def customerFirstName = issue.reporter?.displayName?.replaceAll(/\s.*/, '')

    def causedByIssues = issueLinkManager.getInwardLinks(issue.id).findAll {
        it.issueLinkType.name in ["Problem/Incident"]
    }*.sourceObject

    // this is like this because someone has set up the Cause link type back to front in PS
    if (! causedByIssues) {
        causedByIssues = issueLinkManager.getOutwardLinks(issue.id).findAll {
            it.issueLinkType.name in ["Cause"]
        }*.destinationObject
    }

    def binding = [ // <2>
        currentUser      : currentUser,
        issue            : issue,
        agentFirstname   : agentFirstname,
        customerFirstName: customerFirstName,
        causedByIssues: causedByIssues,
    ]

    def text = getSettings().find {it.name == msgKey}?.template ?: "Template not found"
    def template = engine.createTemplate(text.stripIndent()).make(binding)
    return Response.ok(template.toString()).type(MediaType.TEXT_PLAIN).build()
}
