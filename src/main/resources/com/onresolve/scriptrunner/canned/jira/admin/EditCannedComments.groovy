package com.onresolve.scriptrunner.canned.jira.admin

import com.adaptavist.jsdcc.CannedCommentsService
import com.atlassian.jira.component.ComponentAccessor
import com.atlassian.jira.issue.IssueManager
import com.atlassian.jira.issue.RendererManager
import com.atlassian.jira.issue.fields.renderer.IssueRenderContext
import com.onresolve.scriptrunner.canned.common.AbstractConfiguredItemsCannedScript
import com.onresolve.scriptrunner.canned.jira.utils.ConditionUtils
import com.onresolve.scriptrunner.canned.util.BuiltinScriptErrors
import com.onresolve.scriptrunner.canned.util.SimpleBuiltinScriptErrors
import groovy.util.logging.Log4j
import groovy.xml.MarkupBuilder
import org.apache.commons.collections.map.ListOrderedMap

@Log4j
class EditCannedComments extends AbstractConfiguredItemsCannedScript<Map<String, String>> {

    public static String FIELD_PREVIEW_ISSUE = "FIELD_PREVIEW_ISSUE"
    public static String FIELD_NAME = "name"
    public static String FIELD_DEFAULT = "default"
    public static String FIELD_TEMPLATE = "template"
    public static String FIELD_SNIPPET = "snippet"
    public static String FIELD_CONDITION = "condition"

    private final IssueManager issueManager = ComponentAccessor.getComponent(IssueManager)
    private final CannedCommentsService cannedCommentsService = new CannedCommentsService()

    def rendererManager = ComponentAccessor.getComponent(RendererManager)

    String getName() {
        "Service Desk template comments"
    }

    String getDescription() {
        "Add and edit Service Desk template comments"
    }

    List getCategories() {
        []
    }

    List<Map> getParametersForItem() {
        def exampleConditions = new ListOrderedMap()
        exampleConditions.putAll(ConditionUtils.getConditionParameter()["examples"] as Map)

        exampleConditions.put(1, "Action is <em>Resolve this issue</em>", 'getActionName() == "Resolve this issue"')
        exampleConditions.put(2, "Operation is Link Issue", 'operation == "link-jira-issue" // operation will be one of ' +
            'link-jira-issue, assign-issue, issue-workflow-transition or null' )
        exampleConditions.put(3, "Screen name is <em>Resolve Issue Screen</em>", 'getScreenName() == "Resolve Issue Screen"')

        [
            [
                name       : FIELD_NAME,
                label      : "Name",
                description: "Name of the template, this will appear in the dropdown by the comment area",
            ],
            [
                name       : FIELD_TEMPLATE,
                label      : "Template",
                type       : "textarea",
                description: "The template comment",
                cssClass   : "CodeMirror CodeMirrorSmall",
                lang       : "none",
                examples   : [
                    "Simple"      : 'Hello $customerFirstName...\n\nThis is issue ${issue.key}\n\nBye!',
                    "Kitchen Sink": '''\
                        || Variable || Usage || Type
                        | ${currentUser} | the current user | com.atlassian.jira.user.ApplicationUser
                        | ${issue} | the current issue | com.atlassian.jira.issue.Issue
                        | ${agentFirstname} | the current agent's first name | String
                        | ${customerFirstName} | the reporter's first name | String
                        | ${causedByIssues} | list of inward linked issues with the Problem/Incident link type | List<com.atlassian.jira.issue.Issue>'''.stripIndent(),
                ]
            ],
            [
                name                : FIELD_CONDITION,
                label               : "Condition / Code",
                type                : "textarea",
                description         : "template",
                cssClass            : "CodeMirror CodeMirrorSmall",
                scriptCompileContext: "${CannedCommentsService.PLUGIN_KEY}:${CannedCommentsService.CONFIG_AND_CONDITION_CONTEXT.label}",
                examples            : exampleConditions,
            ],
            [
                name       : FIELD_DEFAULT,
                label      : "Default",
                description: "If selected, and the condition evaluates to true (if present), this template will be " +
                    "prefilled automatically",
                type       : "checkbox",
                values     : [
                    ["true", "Yes, comment will default to this"]
                ]
            ],
            [
                name       : FIELD_SNIPPET,
                label      : "Yes",
                description: "If selected, the comment will be inserted at the cursor, rather than replacing the full comment",
                type       : "checkbox",
                values     : [
                    ["true", "Yes, comment is a snippet"]
                ]
            ],
            [
                name       : FIELD_PREVIEW_ISSUE,
                label      : "Preview Issue Key",
                description: "Issue key for previewing the comment",
            ],
        ]
    }

    @Override
    List<Map<String, String>> load() {
        cannedCommentsService.all()
    }

    @Override
    void save(List<Map<String, String>> list) {
        cannedCommentsService.save(list)
    }

    List<Map> getTableColumns() {
        [
            [
                id  : FIELD_NAME,
                name: "Name",
            ],
            [
                id    : FIELD_TEMPLATE,
                name  : "Template",
                render: { builder, val ->
                    builder.pre(style: "max-height: 100px; overflow-y: auto; white-space: pre-line", val)
                }
            ],
            [
                id  : FIELD_DEFAULT,
                name: "Default",
                render: { builder, val ->
                    if (val) {
                        builder.span(class: "aui-icon aui-icon-small aui-iconfont-approve")
                    }
                }
            ],
        ]
    }

    BuiltinScriptErrors validateItem(Map map, boolean forPreview) {
        def errors = new SimpleBuiltinScriptErrors()
        if (forPreview) {
            String key = map[FIELD_PREVIEW_ISSUE]

            if (!key) {
                errors.addError(FIELD_PREVIEW_ISSUE, "Please enter an issue key to preview with")
            } else {
                if (!issueManager.getIssueObject(key)) {
                    errors.addError(FIELD_PREVIEW_ISSUE, "Can't find this issue")
                }
            }
        }

        if (!map[FIELD_CONDITION] && !map[FIELD_TEMPLATE]) {
            errors.addError(FIELD_TEMPLATE, "Please enter a template - or a condition that will generate a String")
        }

        if (!map[FIELD_NAME]) {
            errors.addError(FIELD_NAME, "Please enter a name for the template")
        }

        errors
    }

    String getDescription(Map<String, String> params, boolean forPreview) {

        def key = params[FIELD_PREVIEW_ISSUE]
        def issue = issueManager.getIssueObject(key)

        def condition = params[FIELD_CONDITION]

        def processedComment = cannedCommentsService.processComment(issue.id, null, null, params)?.replaceAll("\\{CURSOR\\}", "{{|}}")

        def fieldLayoutItem = ComponentAccessor.getFieldLayoutManager().getFieldLayout(issue).getFieldLayoutItem("comment")
        def renderer = rendererManager.getRendererForField(fieldLayoutItem)

        def writer = new StringWriter()
        def builder = new MarkupBuilder(writer)
        def isVisible = processedComment as boolean

        if (condition) {
            builder.p {
                mkp.yieldUnescaped "The condition evaluated to ${isVisible}, the comment ${isVisible ? 'will' : 'will <b>not</b>'} be available for this issue."
            }
            builder.br()
        }

        builder.div {
            mkp.yieldUnescaped(renderer.render(processedComment, new IssueRenderContext(issue)))
        }

        writer
    }
}
