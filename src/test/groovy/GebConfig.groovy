import com.atlassian.sal.api.ApplicationProperties
import com.atlassian.sal.api.UrlMode
import com.onresolve.scriptrunner.runner.ScriptRunnerImpl
import geb.report.PageSourceReporter
import org.openqa.selenium.chrome.ChromeDriver
import org.openqa.selenium.chrome.ChromeOptions
import org.openqa.selenium.firefox.FirefoxDriver
import org.openqa.selenium.remote.DesiredCapabilities
import org.openqa.selenium.remote.RemoteWebDriver

// import org.openqa.selenium.ie.InternetExplorerDriver
// import org.openqa.selenium.phantomjs.PhantomJSDriver

/*
 * To run chrome in headless mode on linux use:
 * xvfb-run --server-args='-screen 0, 1024x768x24' chromedriver
 */

/**
 * Run the tests on a remote linux box
 */
def runRemote = false

/**
 * Host running jira
 */
def jiraHost = "localhost"

/**
 * Host running driver. Set to a remote host to run elsewhere
 */
def chromeDriverHost = "localhost"

// Update the base url to FQDN if running on bamboo agent
def applicationProperties = ScriptRunnerImpl.getOsgiService(ApplicationProperties)

def configuredBaseUrl = applicationProperties.getBaseUrl(UrlMode.ABSOLUTE)

// this is for SauceLabs, because the base URL doesn't contain the FQDN

baseUrl = configuredBaseUrl + "/"

reportsDir = "."

reporter = new PageSourceReporter()

driver = {
    // new FirefoxDriver()

    ChromeOptions options = new ChromeOptions()
    DesiredCapabilities capabilities = DesiredCapabilities.chrome()
    capabilities.setCapability(ChromeOptions.CAPABILITY, options)
    new RemoteWebDriver(new URL("http://$chromeDriverHost:9515"), capabilities)
}