package com.adaptavist.jsdcc

import com.atlassian.jira.bc.project.ProjectCreationData
import com.atlassian.jira.bc.project.ProjectService
import com.atlassian.jira.component.ComponentAccessor
import com.atlassian.jira.config.properties.APKeys
import com.atlassian.jira.issue.Issue
import com.atlassian.jira.project.AssigneeTypes
import com.atlassian.jira.project.template.ProjectTemplateManager
import com.atlassian.jira.project.type.ProjectTypeKey
import com.atlassian.plugin.PluginAccessor
import com.onresolve.scriptrunner.canned.common.admin.SrSpecification
import com.onresolve.scriptrunner.canned.jira.admin.EditCannedComments
import groovy.util.logging.Log4j
import groovyx.net.http.ContentType
import groovyx.net.http.Method
import groovyx.net.http.RESTClient
import org.apache.http.HttpRequest
import org.apache.http.HttpRequestInterceptor
import org.apache.http.protocol.HttpContext
import org.apache.log4j.Level
import spock.lang.Shared

@Log4j
class RestApiSpec extends SrSpecification {

    public static final String TEST_PROJECT_KEY = "SRTESTPRJ"

    @Shared
    RESTClient http

    @Shared
    def commentsRestBasePath = "/jira/rest/jsdcanned/1.0/jsd/comments"

    @Shared
    def issueManager = ComponentAccessor.getIssueManager()

    @Shared
    Issue issue

    @Shared
    def cannedCommentsService = new CannedCommentsService()

    @Shared
    private List<Map> originalConfig

    def setup() {
        // base url - is not set properly when run by maven, so get it from command line
        def baseUrlProp = System.getProperty("baseurl")
        http = new RESTClient(baseUrlProp)
        ComponentAccessor.getApplicationProperties().setString(APKeys.JIRA_BASEURL, baseUrlProp)

        // http.setProxy("localhost", 8888, null)
        http.client.addRequestInterceptor(new HttpRequestInterceptor() {
            void process(HttpRequest httpRequest, HttpContext httpContext) {
                httpRequest.addHeader('Authorization', 'Basic ' + 'admin:admin'.bytes.encodeBase64().toString())
                httpRequest.addHeader('X-Atlassian-Token', "no-check")
            }
        })

        // this works around a stupid bug where a 404 is returned and it tries to parse it as XML
        http.parser.'application/xml' = { response ->
            response.entity.content
        }
    }

    def setupSpec() {

        originalConfig = cannedCommentsService.all()

        log.getLoggerRepository().getLogger("com.onresolve").setLevel(Level.DEBUG)
        log.getLoggerRepository().getLogger("com.adaptavist").setLevel(Level.DEBUG)

        def user = ComponentAccessor.getJiraAuthenticationContext().loggedInUser
        def projectService = ComponentAccessor.getComponent(ProjectService)

        def templateManager= ComponentAccessor.getComponent(ProjectTemplateManager)
        def projectTemplateKey = "com.atlassian.servicedesk:classic-service-desk-project"

        if(!templateManager.getProjectTemplates()*.key*.getKey().contains(projectTemplateKey)){
            projectTemplateKey = "com.atlassian.servicedesk:basic-service-desk-project"
        }
        if (!projectService.getProjectByKey(TEST_PROJECT_KEY).get()) {
            def creationData = new ProjectCreationData.Builder().with {
                withName("Testing Project")
                withKey(TEST_PROJECT_KEY)
                withDescription("project for testing")
                withLead(user)
                withUrl(null)
                withAssigneeType(AssigneeTypes.PROJECT_LEAD)
                withType(new ProjectTypeKey("service_desk"))
                withProjectTemplateKey(projectTemplateKey)
            }.build()

            final ProjectService.CreateProjectValidationResult result = projectService.validateCreateProject(user, creationData)
            assert !result.errorCollection.hasAnyErrors()

            projectService.createProject(result)
        }

        issue = issueManager.getIssueObject("$TEST_PROJECT_KEY-1")
    }

    def cleanupSpec() {
        cannedCommentsService.save(originalConfig)
    }

    def "edit spec"() {
        setup:

        def editCannedComments = new EditCannedComments()

        when:
        def errors = editCannedComments.validateItem([:], false)

        then:
        errors.hasAnyErrors()

        when:

        def issue = issueManager.getIssueObject("$TEST_PROJECT_KEY-1")
        errors = editCannedComments.validateItem([
            (EditCannedComments.FIELD_TEMPLATE)     : "foo",
            (EditCannedComments.FIELD_PREVIEW_ISSUE): issue.key,
            (EditCannedComments.FIELD_NAME)         : "test",
        ], false)

        then:
        !errors.hasAnyErrors()

        when:
        def comment = cannedCommentsService.processComment(issue.id, null, null, [
            name     : "aa",
            template : 'AA $foo',
            condition: 'config = [foo: "bar"]; true',
        ])

        then:
        comment == "AA bar"
    }

    def processComment() {
        expect:
        cannedCommentsService.processComment(issue.id, null, null, [template: "foo \${issue.id}"]) == "foo ${issue.id}".toString()
        cannedCommentsService.processComment(issue.id, null, null, [
            template : "\$foo",
            condition: 'config = [foo: "bar"]; true',
        ]) == "bar"
    }


    def "test comments rest api"() {
        setup:

        cannedCommentsService.save([])
        cannedCommentsService.add([
            name     : "aa",
            template : 'AA $foo',
            condition: 'config = [foo: "bar"]; true',
        ])

        def response = http.request(Method.GET, ContentType.JSON) {
            uri.path = "$commentsRestBasePath/keys"
            uri.query = [issueId: issue.id]
        }

        expect:
        response.data == [
            [
                id     : 0,
                name   : "aa",
                default: false,
                snippet: false,
            ]
        ]

        when:
        response = http.request(Method.GET, ContentType.JSON) {
            uri.path = "$commentsRestBasePath/comment".toString()
            uri.query = [id: 0, issueId: issue.id]
        }

        then:
        response.data.template == "AA bar"
    }

    def "test default comment"() {
        setup:

        cannedCommentsService.save([
            [
                name     : "aa",
                template : 'AA',
                condition: 'false',
                default  : true,
            ],
            [
                name     : "bb",
                template : 'BB',
                condition: 'true',
                default  : true,
            ]
        ])

        def response = http.request(Method.GET, ContentType.JSON) {
            uri.path = "$commentsRestBasePath/keys"
            uri.query = [issueId: issue.id]
        }

        expect:
        response.data == [
            [
                id     : 1,
                name   : "bb",
                default: true,
                snippet: false,
            ]
        ]
    }

    def "test getting screen and action"() {

        setup:
        def workflowManager = ComponentAccessor.getWorkflowManager()
        def workflow = workflowManager.getWorkflow(issue)
        def versionSafe = false
        def action = workflow.getActionsByName("Resolve this issue").find { it.parent.name == "Waiting for customer" }
        if(!action){
            versionSafe = true
            action = workflow.getActionsByName("Resolve this issue")[0]
        }

        when:
        cannedCommentsService.save([
            [
                name     : "aa",
                template : 'AA',
                condition: "actionId == ${action.id}",
                default  : true,
            ],
        ])

        then:
        cannedCommentsService.filter(issue.id, action.id, null)

        when:
        cannedCommentsService.save([
            [
                name     : "aa",
                template : 'AA',
                condition: 'getActionName() == "Resolve this issue"',
                default  : true,
            ],
        ])

        then:
        cannedCommentsService.filter(issue.id, action.id, null)

        if(!versionSafe) {
            when:
            cannedCommentsService.save([
                [
                    name     : "aa",
                    template : 'AA',
                    condition: 'getScreenName().startsWith("Resolve Issue Screen")',
                    default  : true,
                ],
            ])

            then:
            cannedCommentsService.filter(issue.id, action.id, null)
        }
        // possible operations are: link-jira-issue, assign-issue, action= null and formId = null - then edit.
        // otherwise workflow transitions are issue-workflow-transition

        when:
        cannedCommentsService.save([
            [
                name     : "aa",
                template : 'AA',
                condition: 'operation == "link-jira-issue"',
                default  : true,
            ],
        ])

        then:
        cannedCommentsService.filter(issue.id, null, "link-jira-issue")
    }
}
