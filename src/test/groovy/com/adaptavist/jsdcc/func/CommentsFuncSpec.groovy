package com.adaptavist.jsdcc.func

import com.onresolve.scriptrunner.canned.common.admin.ScriptRunnerTestRunner
import geb.driver.CachingDriverFactory
import geb.spock.GebReportingSpec
import org.junit.runner.RunWith
import org.openqa.selenium.Keys

@RunWith(ScriptRunnerTestRunner)
class CommentsFuncSpec extends GebReportingSpec {

    def setupSpec() {
        CachingDriverFactory.clearCacheAndQuitDriver()
    }

    def setup() {
        browser.go("browse/SRTESTPRJ-1", os_username: "admin", os_password: "admin")
    }

    def "comments in a full screen issue"() {
        setup:

        $("#sd-comment-collapsed-textarea").click()

        // this tests that the default comment works and should not be removed
        waitFor {
            $("#comment").value().toString().contains("Regards, admin")
        }

        // this mode is not tested after 7.3
        if ($("#sd-comment-preview_link")) {
            $("#comment").value("Some other string")
            $("#sd-comment-preview_link").click()
            $("div.sd-comment-preview").value()
            waitFor {
                $("div.sd-comment-preview p").text() == "Some other string"
            }
            $("#sd-comment-preview_link").click()
        } else {
            // previous run needs to have set visual mode, otherwise text mode is already selected here,
            // and clicking it just disables both buttons for some reason
            $("#aui-uid-2").click() // text mode
            $("#comment").value("Some other string")
            $("#aui-uid-1").click() // visual mode
            then:
            $("#comment").text().contains("Some other string")
        }

        chooseComment("0")

        waitFor {
            $("#comment").value().toString().contains("application server logs")
        }

        $("button.cancel").click()

        $(".sd-comment-container").click()

        waitFor {
            $("button.canned-responses").displayed
        }

        cleanup:
        $("button.cancel").click()
    }

    private void chooseComment(String commentId) {
        // set a comment
        $("button.canned-responses").click()
        $("#comments-dropdown li a", rel: commentId).click()
    }

    def "comments in link issue dialog"() {
        setup:
        $("#opsbar-operations_more").click()

        when:
        $("#link-issue").click()

        waitFor {
            $("form#link-jira-issue")
        }

        then:
        $("#comment").value() == ""

        when: "click on either of the comment entry boxes (visual or text)"
        try {
            $("#comment").click()
        }
        catch(Exception e){
            $("#aui-uid-2").click() // switch to text mode for testing purposes
            $("#comment").click()
        }
        then:
        waitFor {
            $("#comment").value().toString().contains("Regards, admin")
        }

        $("button.canned-responses").displayed

        when:
        chooseComment("0")

        then:
        waitFor {
            $("#comment").value().toString().contains("application server logs")
        }

        when: "set other comment and switch to internal"
        $("#comment").value("Some other string")
        $("li.js-sd-internal-comment").click()

        then: "comment retained"
        $("#comment").value("Some other string")

        when: "try to submit"
        $("input", type: "submit", name: "Link").click()

        then: "get an error because no link is specified, but the comment remains"
        waitFor {
            $("#comment").value().toString().contains("Some other string")
        }

        cleanup:
        $("a.cancel").click()
    }

    def "comments in comment dialog"() {
        setup:

        def activeElement = browser.driver.switchTo().activeElement()
        activeElement.sendKeys(".")

        waitFor {
            $("#shifter-dialog-field").displayed
        }

        when:
        activeElement = browser.driver.switchTo().activeElement()
        sleep(10)
        activeElement.sendKeys("Comment" + Keys.ENTER)
        sleep(40)
        chooseComment("3")
        sleep(40)
        then:
        waitFor {
            $("#comment").value().toString().contains("Regards, admin")
        }

        when:
        $("#comment-add-submit").click()

        then:
        1
    }

    def "comments in resolve dialog"() {
        when:
        $(".issueaction-workflow-transition").first().click()

        then:
        waitFor {
            $("#comment").value() == ""
        }

        when:
        chooseComment("0")

        then:
        waitFor {
            $("#comment").value().toString().contains("Please could you add the application server logs")
        }

        cleanup:
        $("a.cancel").click()
    }
}
