package com.adaptavist.jsdcc

import com.adaptavist.jsdcc.upgrade.AddSamplesUpgradeTask01
import com.atlassian.jira.component.ComponentAccessor
import com.atlassian.jira.config.FeatureManager
import com.atlassian.sal.api.pluginsettings.PluginSettingsFactory
import com.atlassian.sal.api.upgrade.PluginUpgradeManager
import com.onresolve.scriptrunner.canned.common.admin.SrSpecification
import com.onresolve.scriptrunner.runner.ScriptRunnerImpl
import groovy.util.logging.Log4j

@Log4j
class UpgradeSpec extends SrSpecification {

    public static final String BUILD = ":build"

    def "reset plugin build number"() {
        setup:

        // Look for PluginUpgrader source in sal-core-3.0.3-sources.jar

        def pluginSettingsFactory = ScriptRunnerImpl.getOsgiService(PluginSettingsFactory)
        def pluginSettings = pluginSettingsFactory.createGlobalSettings()

        def buildNumberKey = CannedCommentsService.PLUGIN_KEY + BUILD
        final String val = (String) pluginSettings.get(buildNumberKey)

        log.debug(val)

        pluginSettings.remove(buildNumberKey)
    }

    def "run plugin upgrade"() {
        setup:

        def pluginUpgradeManager = ComponentAccessor.getOSGiComponentInstanceOfType(PluginUpgradeManager)
        log.debug pluginUpgradeManager.upgrade()
    }

    def "check conversational mode is disabled"() {
        setup:
        def featureManager = ComponentAccessor.getComponent(FeatureManager)

        expect:
        featureManager.isEnabled("sd.email.conversational.notification.disabled")
    }

    def "just run upgrade task"() {
        setup:
        def service = new CannedCommentsService()
        service.save([])

        new AddSamplesUpgradeTask01().doUpgrade()
    }
}
